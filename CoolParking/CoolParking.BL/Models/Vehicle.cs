﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle()
        {
            Id = GenerateRandomRegistrationPlateNumber();
            VehicleType = VehicleType.PassengerCar;
            Balance = 0;
        }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            string pattern = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";

            if(!Regex.IsMatch(id, pattern))
            {
                throw new ArgumentException("Wrong id! Pattern: AA-0000-AA.");
            }
            if(balance < 0)
            {
                throw new ArgumentException("Balance cant be less than zero.");
            }
            Id =  id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            string plateNumber = "";
            for (int i = 0; i < 4; i++)
            {
                if (i == 2)
                {
                    plateNumber += '-';
                    for (int j = 0; j < 4; j++)
                    {
                        plateNumber += random.Next(0, 9);
                    }
                    plateNumber += '-';
                }
                plateNumber += (char)random.Next('A', 'Z');
            }
            return plateNumber;
        }

        public override string ToString()
        {
            return $"Номер: {Id}, Тип: {VehicleType.ToString()}, Баланс: {Balance}";
        }
    }
}