﻿using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.UI.Pages
{
    class TransactionsHistoryPage : Page
    {
        private ParkingService _parkingService;
        public TransactionsHistoryPage(EasyConsole.Program program) : base("История транзакций (Transactions.log)", program)
        {
            _parkingService = CoolParking.UI.Program.parkingService;
        }
        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            Output.WriteLine("История транзакций:");
            string transactions = _parkingService.ReadFromLog();
            Output.WriteLine(transactions);

            Input.ReadString("Нажмите [Enter] чтобы вернуться в меню.");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
