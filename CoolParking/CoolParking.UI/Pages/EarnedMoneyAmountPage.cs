﻿using CoolParking.BL.Services;
using EasyConsole;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.UI.Pages
{
    class EarnedMoneyAmountPage : Page
    {
        private ParkingService _parkingService;
        public EarnedMoneyAmountPage(EasyConsole.Program program) : base("Сумма заработанных денег за текущий период (до записи в лог)", program)
        {
            _parkingService = CoolParking.UI.Program.parkingService;
        }

        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);
            decimal sum = 0;
            var transactions = _parkingService.GetLastParkingTransactions();
            for (int i = 0; i < transactions.Length; i++)
            {
                sum += transactions[i].Sum;
            }
            Output.WriteLine($"За текущий период заработано: {sum}");

            Input.ReadString("Нажмите [Enter] чтобы вернуться в меню.");
            await Program.NavigateHome(cancellationToken);
        }
    }
}