﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.UI.Pages
{
    class PutVehicleInParkingPage : Page
    {
        private ParkingService _parkingService;
        public PutVehicleInParkingPage(EasyConsole.Program program) : base("Поставить Тр. средство на Паркинг", program)
        {
            _parkingService = CoolParking.UI.Program.parkingService;
        }
        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            Vehicle vehicle;
            Output.WriteLine("Установить тр. средство на паркинг:");
            vehicle = await ObtainInputVehicle(cancellationToken);
            _parkingService.AddVehicle(vehicle);
            Output.WriteLine("Тр. средство запарковано.");

            Input.ReadString("Нажмите [Enter] чтобы вернуться в меню.");
            await Program.NavigateHome(cancellationToken);
        }

        private async Task<Vehicle> ObtainInputVehicle(CancellationToken cancellationToken)
        {
            Vehicle inputVehicle;
            var vehicleId = Input.ReadString("Укажите номер Вашего тр. средства (AA-0000-AA):");
            var vehicleType = await Input.ReadEnum<VehicleType>("Выберите тип Вашего тр. средства:", cancellationToken);
            Output.WriteLine("Установите баланс Вашого тр. средства:");
            var vehicleBalance = Convert.ToDecimal(Input.ReadInt());
            try
            {
                inputVehicle = new Vehicle(vehicleId, vehicleType, vehicleBalance);
            }
            catch (Exception e)
            {
                Output.WriteLine($"Ошибка при вводе данных: {e.Message}");
                inputVehicle = await ObtainInputVehicle(cancellationToken);
            }
            return inputVehicle;
        }
    }
}
