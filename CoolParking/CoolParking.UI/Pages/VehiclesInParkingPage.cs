﻿using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.UI.Pages
{
    class VehiclesInParkingPage : Page
    {
        private ParkingService _parkingService;
        public VehiclesInParkingPage(EasyConsole.Program program) : base("Список Тр. средств находящихся на Паркинге", program)
        {
            _parkingService = CoolParking.UI.Program.parkingService;
        }

        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            Output.WriteLine("Тр. средств находящиеся на Паркинге:");
            foreach (var vehicle in _parkingService.GetVehicles())
            {
                Output.WriteLine(vehicle.ToString());
            }

            Input.ReadString("Нажмите [Enter] чтобы вернуться в меню.");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
