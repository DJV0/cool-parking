﻿using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoolParking.UI;
using CoolParking.BL.Services;

namespace CoolParking.UI.Pages
{
    class ParkingBalancePage : Page
    {
        private ParkingService _parkingService;
        public ParkingBalancePage(EasyConsole.Program program) : base("Баланс парковки",program)
        {
            _parkingService =  CoolParking.UI.Program.parkingService;
        }

        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);
            Output.WriteLine($"Баланс паркинга: {_parkingService.GetBalance()} ");
            Input.ReadString("Нажмите [Enter] чтобы вернуться в меню.");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
