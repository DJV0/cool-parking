﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyConsole;

namespace CoolParking.UI.Pages
{
    class MainPage : MenuPage
    {
        public MainPage(EasyConsole.Program program, List<Option> menuItems) : base("Меню", program, menuItems.ToArray())
        {
        }
    }
}
