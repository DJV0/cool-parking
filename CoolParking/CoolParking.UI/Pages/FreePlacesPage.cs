﻿using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.UI.Pages
{
    class FreePlacesPage : Page
    {
        private ParkingService _parkingService;
        public FreePlacesPage(EasyConsole.Program program) : base("Свободные/занятые места на парковке", program)
        {
            _parkingService = CoolParking.UI.Program.parkingService;
        }

        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            Output.WriteLine("Мест на парковке:");
            Output.WriteLine($"-- свободно: {_parkingService.GetFreePlaces()}");
            Output.WriteLine($"-- занято: {_parkingService.GetCapacity() - _parkingService.GetFreePlaces()}");

            Input.ReadString("Нажмите [Enter] чтобы вернуться в меню.");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
