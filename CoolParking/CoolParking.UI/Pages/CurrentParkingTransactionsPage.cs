﻿using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.UI.Pages
{
    class CurrentParkingTransactionsPage : Page
    {
        private ParkingService _parkingService;
        public CurrentParkingTransactionsPage(EasyConsole.Program program) : base("Транзакции за текущий период (до записи в лог)", program)
        {
            _parkingService = CoolParking.UI.Program.parkingService;
        }

        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            Output.WriteLine("Транзакции за текущий период:");
            var transactions = _parkingService.GetLastParkingTransactions();
            for (int i = 0; i < transactions.Length; i++)
            {
                Output.WriteLine(transactions[i].ToString());
            }

            Input.ReadString("Нажмите [Enter] чтобы вернуться в меню.");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
