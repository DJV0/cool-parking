﻿using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.UI.Pages
{
    class TopUpVehiclePage : Page
    {
        private ParkingService _parkingService;
        public TopUpVehiclePage(EasyConsole.Program program) : base("Пополнить баланс конкретного Тр. средства", program)
        {
            _parkingService = CoolParking.UI.Program.parkingService;
        }
        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            Output.WriteLine("Пополнить баланс Тр. средства:");
            var vehicleId = Input.ReadString("Укажите номер тр. средства (AA-0000-AA) баланс которого вы хотите пополнить:");
            var sum = await ObtainSum();
            try
            {
                _parkingService.TopUpVehicle(vehicleId, sum);
                Output.WriteLine("Баланс пополнен.");

            }
            catch (Exception e)
            {
                Output.WriteLine($"Ошибка: {e.Message}");
            }

            Input.ReadString("Нажмите [Enter] чтобы вернуться в меню.");
            await Program.NavigateHome(cancellationToken);
        }

        private async Task<decimal> ObtainSum()
        {
            decimal sum = 0;
            Output.DisplayPrompt("Укажите сумму:");
            decimal input = Convert.ToDecimal(Input.ReadInt());
            if(input > 0)
            {
                sum = input;
            }
            else
            {
                Output.DisplayPrompt("Сумма не верна.");
                input = await ObtainSum();
            }
            return sum;
        }
    }
}
