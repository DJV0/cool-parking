﻿using CoolParking.BL.Services;
using EasyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.UI.Pages
{
    class PickUpVehicleFromParkingPage : Page
    {
        private ParkingService _parkingService;
        public PickUpVehicleFromParkingPage(EasyConsole.Program program) : base("Забрать транспортное средство с Паркинга", program)
        {
            _parkingService = CoolParking.UI.Program.parkingService;
        }
        public override async Task Display(CancellationToken cancellationToken)
        {
            await base.Display(cancellationToken);

            var vehicleId = Input.ReadString("Укажите номер тр. средства (AA-0000-AA) которое вы хотите забрать:");
            try
            {
                _parkingService.RemoveVehicle(vehicleId);
            }
            catch (Exception e)
            {
                Output.WriteLine($"Ошибка: {e.Message}");
            }

            Input.ReadString("Нажмите [Enter] чтобы вернуться в меню.");
            await Program.NavigateHome(cancellationToken);
        }
    }
}
