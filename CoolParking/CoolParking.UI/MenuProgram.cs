﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.UI.Pages;
using EasyConsole;

namespace CoolParking.UI
{
    class MenuProgram : EasyConsole.Program
    {
        private List<Option> menuItems;

        public MenuProgram() : base("Cool Parking", breadcrumbHeader: true)
        {
            menuItems = new List<Option>()
            {
                new Option("Вывести текущий баланс Парковки", NavigateTo<ParkingBalancePage>),
                new Option("Вывести сумму заработанных денег за текущий период (до записи в лог)", NavigateTo<EarnedMoneyAmountPage>),
                new Option("Вывести количество свободных/занятых мест на парковке", NavigateTo<FreePlacesPage>),
                new Option("Вывести все Транзакции Парковки за текущий период (до записи в лог)", NavigateTo<CurrentParkingTransactionsPage>),
                new Option("Вывести историю транзакций (из файла Transactions.log)", NavigateTo<TransactionsHistoryPage>),
                new Option("Вывести список Тр. средств находящихся на Паркинге", NavigateTo<VehiclesInParkingPage>),
                new Option("Поставить Тр. средство на Паркинг", NavigateTo<PutVehicleInParkingPage>),
                new Option("Забрать транспортное средство с Паркинга", NavigateTo<PickUpVehicleFromParkingPage>),
                new Option("Пополнить баланс конкретного Тр. средства", NavigateTo<TopUpVehiclePage>),
            };

            AddPage(new MainPage(this, menuItems));
            AddPage(new ParkingBalancePage(this));
            AddPage(new EarnedMoneyAmountPage(this));
            AddPage(new FreePlacesPage(this));
            AddPage(new CurrentParkingTransactionsPage(this));
            AddPage(new TransactionsHistoryPage(this));
            AddPage(new VehiclesInParkingPage(this));
            AddPage(new PutVehicleInParkingPage(this));
            AddPage(new PickUpVehicleFromParkingPage(this));
            AddPage(new TopUpVehiclePage(this));

            SetPage<MainPage>();
        }
    }
}
