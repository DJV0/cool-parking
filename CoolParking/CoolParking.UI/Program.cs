﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.UI
{
    class Program
    {
        private static ParkingService _parkingService;
        public static ParkingService parkingService
        {
            get
            {
                return _parkingService;
            }
        }
        static Task Main(string[] args)
        {
            TimerService withdrawTimer = new TimerService(Settings.ChargeInterval);
            TimerService logTimer = new TimerService(Settings.LoggingInterval);
            LogService logService = new LogService(Settings.LogPath);
            _parkingService = new ParkingService(withdrawTimer, logTimer, logService);
            return new MenuProgram().Run(CancellationToken.None);
        }
    }
}
